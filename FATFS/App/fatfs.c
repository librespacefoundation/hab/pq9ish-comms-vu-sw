/**
  ******************************************************************************
  * @file   fatfs.c
  * @brief  Code for fatfs applications
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */

#include "fatfs.h"

uint8_t retSD;    /* Return value for SD */
char SDPath[4];   /* SD logical drive path */
FATFS SDFatFS;    /* File system object for SD logical drive */
FIL SDFile;       /* File object for SD */

/* USER CODE BEGIN Variables */
extern balloon_system_conf_t system_conf;
/* USER CODE END Variables */

void
MX_FATFS_Init(void)
{
	/*## FatFS: Link the SD driver ###########################*/
	retSD = FATFS_LinkDriver(&SD_Driver, SDPath);

	/* USER CODE BEGIN Init */

	return ; //TODO:
	if (retSD != 0)
		Error_Handler();

	if (f_mount(&SDFatFS, (TCHAR const *)SDPath, 1) != FR_OK)
		return;
//    Error_Handler();

	read_conf_file();

#ifdef ERASE_LOG_ON_START
	f_unlink("DATA/SENSOR.csv");
#endif
	retSD = f_mkdir("SYS");
	if (retSD != FR_OK && retSD != FR_EXIST)
		Error_Handler();

	retSD = f_mkdir("DATA");
	if (retSD != FR_OK && retSD != FR_EXIST)
		Error_Handler();
	/* USER CODE END Init */
}

/**
  * @brief  Gets Time from RTC
  * @param  None
  * @retval Time in DWORD
  */
DWORD
get_fattime(void)
{
	/* USER CODE BEGIN get_fattime */
	return 0;
	/* USER CODE END get_fattime */
}

/* USER CODE BEGIN Application */

void
read_conf_file()
{
	int res;
	char line[20], param[20];
	res = f_open(&SDFile, "SYS/init.txt", FA_READ);
	if (res != FR_OK)
		return;

	//read
	while (f_gets(line, sizeof line, &SDFile)) {
		sscanf(line, "%s ", param);
		parameter_read(param, line);
	}

	res = f_close(&SDFile);
}

void
parameter_read(char *param, char *line)
{
	if (strcmp(param, "CALLSIGN") == 0) {
		sscanf(line, "CALLSIGN %s", system_conf.callsign);
	}
	if (strcmp(param, "TX_INT_MS") == 0) {
		sscanf(line, "TX_INT_MS %hu", &system_conf.tx_interval_ms);
	}
	if (strcmp(param, "QNH") == 0) {
		sscanf(line, "QNH %hu", &system_conf.QNH);
	}
	if (strcmp(param, "PYRO_ALT") == 0) {
		sscanf(line, "PYRO_ALT %hu", &system_conf.pyro_alt);
	}
	if (strcmp(param, "HI_LAND_POINT") == 0) {
		sscanf(line, "HI_LAND_POINT %hu", &system_conf.highest_land_point);
	}
}
/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
