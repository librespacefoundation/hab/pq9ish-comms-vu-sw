/*
 *  PQ9ISH COMMS: comms module, part of an open source pocketqube stack
 *
 *  Copyright (C) 2020, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef USE_AX25
#define CALLSIGN_STATION                (uint8_t*) "XXXXXX/P"
#define CALLSIGN_DESTINATION            (uint8_t*) ""
#define TX_DATA_TYPE                    ax25_data_t

#endif

#ifdef USE_APRS
#define APRS_UHF                        432500000
#define TX_DATA_TYPE                    aprs_data_t
#endif

#ifndef TX_DATA_TYPE
#define TX_DATA_TYPE                    char[200]
#endif

#define RX_FREQ_HZ                      APRS_UHF
#ifdef USE_APRS
#define TX_FREQ_HZ                      APRS_UHF
#else
#define TX_FREQ_HZ                      432500000
#endif

/* Reference Oscillator frequency */
#if PQ9ISH_DEV_BOARD
#define XTAL_FREQ_HZ                    48000000
#else
#define XTAL_FREQ_HZ                    26000000
#endif
