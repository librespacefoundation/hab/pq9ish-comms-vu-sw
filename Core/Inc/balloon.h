/*
 *  A PQ9 based Weather Station
 *
 *  Copyright (C) 2017 Libre Space Foundation
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BALOON_H_
#define BALOON_H_

#include <stdint.h>
#include "weather.h"

typedef struct {
	uint32_t sysTick;
	float battVolt;
	float inputVolt;
} pq9ish_system_status_t;

typedef struct {
	char callsign[10];
	uint16_t pyro_alt;
	uint16_t highest_land_point;
	uint16_t tx_interval_ms;
	uint16_t QNH;
} balloon_system_conf_t;


#endif /* BALLOON_H_ */
