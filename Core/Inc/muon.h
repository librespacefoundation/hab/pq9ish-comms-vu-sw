
#ifndef MUON_H_
#define MUON_H_

#define PQ_MUON_NODE_BASEID 0x02
#define PQ_MUON_NODE_EXTID 0x0
//#define PQ_MESSAGE_MASK 0x3F

#define PQ_MUON_MUON 0
#define PQ_MUON_FREQUENCY 0x01
#define PQ_MUON_EXTERNAL_TEMPERATURE 0x02

///
#define MUONS_PER_PACKET 2
#define FREQUENCY_PER 2 //100

/*
typedef enum {
  PQ_MUON_MUON = 0,
  PQ_MUON_FREQUENCY,
  PQ_MUON_EXTERNAL_TEMPERATURE
} muon_messages_t;
*/

#endif /* MUON_H_ */
