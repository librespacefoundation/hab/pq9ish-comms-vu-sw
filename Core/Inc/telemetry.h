/*
 *
 *  PQ9ISH COMMS: comms module, part of an open source pocketqube stack
 *
 *  Copyright (C) 2020, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 *  @file telemetry.h
 *
 *  @date Jul 21, 2020
 *  @brief Telemetry frame definitions
 */

#ifndef INC_TELEMETRY_H_
#define INC_TELEMETRY_H_

#include "pq9ish.h"
#include <stdint.h>


uint8_t
telemetry_tx_power_index(int8_t tx_power);

void
telemetry_tx_callback(struct pq9ish *q);

/**
 * Telemetry transmit. Passes the higher-level telemetry frame to the OSDLP
 * layer for encapsulation.
 * @param pkt the payload of the telemetry frame
 * @param length the length of the payload
 * @param vcid the VCID
 * @param meta the metadata attached to this frame
 * @return 0 zero for success, negative otherwise
 */
int
transmit_tm(const uint8_t *pkt, uint16_t length, uint8_t vcid,
            const struct tx_frame_metadata *meta);

/**
 * Retrieves the metadata for tm transmission
 *
 * @param q the pq9ish struct
 * @param meta the buffer where the metadata will be stored
 */
int
retrieve_meta(struct pq9ish *q, struct tx_frame_metadata *meta);

size_t
telemetry_basic_frame(const struct pq9ish *q, uint8_t *buffer);


#endif /* INC_TELEMETRY_H_ */
