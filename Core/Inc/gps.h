/*
 * gps.h
 *
 *  Created on: Mar 30, 2019
 *      Author: george
 */

#include <main.h>

#ifndef GPS_H_
#define GPS_H_

#define GpsRxBufferSize 40
#define INIT_LAT "0000.00N"
#define INIT_LON "00000.00E"
#define APRS_LON_STR_LEN                (9)
#define APRS_LAT_STR_LEN                (8)

typedef struct {
	uint8_t timeStamp[9];
	uint8_t lat[9];
	uint8_t lat_dir;
	uint8_t lon[9];
	uint8_t lon_dir;
	uint8_t alt_txt[9];
	uint16_t alt;
	uint8_t pos_status;
	uint8_t date[6];
} GPS_Data;

uint8_t
isSeperator(uint8_t c);
void
parseToGpsStruct(uint8_t *sentence, uint8_t size, GPS_Data *instance,
                 uint8_t type);
void
gps_init();

void
Gps_Recieve_Callback(UART_HandleTypeDef *uartHandle);

#endif /* GPS_H_ */
