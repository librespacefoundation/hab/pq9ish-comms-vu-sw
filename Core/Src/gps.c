#include <string.h>
#include "gps.h"
#include "weather.h"
#include "utils.h"



extern GPS_Data gps_data;
extern uint8_t GpsRxSwapper ;
extern uint8_t GpsRxBuffer[2][GpsRxBufferSize];
uint8_t sentenceSwapper = 0;
uint8_t sentenceCounter = 0;
uint8_t sentence[2][80];

extern weather_data_t weather_data;
extern RTC_TimeTypeDef sTime;
extern RTC_DateTypeDef sDate;
extern RTC_HandleTypeDef hrtc;


void
gps_init()
{
	memset(&gps_data.alt_txt, '0', sizeof(gps_data.alt_txt));
	sprintf(gps_data.lat, INIT_LAT, APRS_LAT_STR_LEN);
	sprintf(gps_data.lon, INIT_LON, APRS_LON_STR_LEN);
}

uint8_t
isSeperator(uint8_t c)
{
	switch (c) {
		case '\n':
			return 2;
			break;
		case '\r':
			return 2;
			break;
		case ',':
			return 1;
			break;
		default:
			return 0;
			break;
	}
}

void
parseToGpsStruct(uint8_t *sentence, uint8_t size, GPS_Data *instance,
                 uint8_t type)
{
	//TYPE 0 = GGA
	//TYPE 1 = RMC
	uint16_t curr_cntr = 0;
	uint16_t last_cntr = 0;
	uint8_t field = 0;
	\
	if (type == 0) {
		for (int i = 0; i < size; i++) {
			switch (isSeperator(sentence[i])) {
				case 0:
					break;
				case 1:
					last_cntr = curr_cntr;
					curr_cntr = i;
					field++;
					for (int j = last_cntr + 1; j < curr_cntr; j++) {
						switch (field) {
							case 2:
								gps_data.timeStamp[j - last_cntr - 1] = sentence[j];
								break;
							case 3:
								gps_data.lat[j - last_cntr - 1] = sentence[j];
								break;
							case 4:
								gps_data.lat_dir = sentence[j];
								break;
							case 5:
								gps_data.lon[j - last_cntr - 1] = sentence[j];
								break;
							case 6:
								gps_data.lon_dir = sentence[j];
								break;
							case 10:
								gps_data.alt_txt[j - last_cntr - 1] = sentence[j];
								break;
						}
					}
					break;
				default:
					break;
			}
		}
	}
	if (type == 1) {
		for (int i = 0; i < size; i++) {
			switch (isSeperator(sentence[i])) {
				case 0:
					break;
				case 1:
					last_cntr = curr_cntr;
					curr_cntr = i;
					field++;
					for (int j = last_cntr + 1; j < curr_cntr; j++) {
						switch (field) {
							case 3:
								gps_data.pos_status = sentence[j];
								break;
							case 10:
								gps_data.date[j - last_cntr - 1] = sentence[j];
								break;
						}
					}
					break;
				default:
					break;
			}
		}
	}
}

void
Gps_Recieve_Callback(UART_HandleTypeDef *uartHandle)
{
	uint16_t dh, mm, sy;
	GpsRxSwapper++;
	HAL_UART_Receive_IT(&uartHandle, GpsRxBuffer[GpsRxSwapper % 2],
	                    sizeof(GpsRxBuffer[0]));
	GpsRxSwapper--;
	//Handle received array
	for (uint8_t i = 0; i < GpsRxBufferSize; i++) {
		uint8_t c = GpsRxBuffer[GpsRxSwapper % 2][i];
		if (c == '$') {
			//CODE TO EXECUTE WHEN sentence[sentenceSwapper%2] IS FINISHED
			if ((sentence[sentenceSwapper % 2][3] == 'G')
			    && (sentence[sentenceSwapper % 2][4] == 'G')
			    && (sentence[sentenceSwapper % 2][5] == 'A')) {
				parseToGpsStruct(sentence[sentenceSwapper % 2], sentenceCounter,
				                 &gps_data, 0);
			}
			if ((sentence[sentenceSwapper % 2][3] == 'R')
			    && (sentence[sentenceSwapper % 2][4] == 'M')
			    && (sentence[sentenceSwapper % 2][5] == 'C')) {
				parseToGpsStruct(sentence[sentenceSwapper % 2], sentenceCounter,
				                 &gps_data, 1);

			}
			if (gps_data.pos_status == 'A') {
				/*
				memcpy(hrf.haprs.lat, gps_data.lat, APRS_LAT_STR_LEN);
				hrf.haprs.lat[APRS_LAT_STR_LEN - 1] = gps_data.lat_dir;
				memcpy(hrf.haprs.lon, gps_data.lon, APRS_LON_STR_LEN);
				hrf.haprs.lon[APRS_LON_STR_LEN - 1] = gps_data.lon_dir;
				sscanf(&gps_data.alt_txt, "%hu", &gps_data.alt);
				*/
				weather_data.system_state |= BIT(4);
			} else {
				weather_data.system_state &= ~BIT(4);
			}

			if (strlen(gps_data.timeStamp) > 1) {
				sscanf(gps_data.date, "%2hu%2hu%2hu", &dh, &mm, &sy);
				sDate.Date = (uint8_t)dh;
				sDate.Month = (uint8_t)mm;
				sDate.Year = (uint8_t)sy;
				HAL_RTC_SetDate(&hrtc, &sDate, RTC_FORMAT_BIN);

				sscanf(gps_data.timeStamp, "%2hu%2hu%2hu", &dh, &mm, &sy);
				sTime.Hours = (uint8_t)dh;
				sTime.Minutes = (uint8_t)mm;
				sTime.Seconds = (uint8_t)sy;
				HAL_RTC_SetTime(&hrtc, &sTime, RTC_FORMAT_BIN);
			}
			//END CODE TO EXECUTE
			sentenceSwapper++;
			sentenceCounter = 0;
		}
		sentence[sentenceSwapper % 2][sentenceCounter] = c;
		sentenceCounter++;
	}
	//End Handle received array
	GpsRxSwapper++;
}



