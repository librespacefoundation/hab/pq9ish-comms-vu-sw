/*
 *  PQ9ISH COMMS: comms module, part of an open source pocketqube stack
 *
 *  Copyright (C) 2020, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "fsm.h"
#include "watchdog.h"
#include "error.h"
#include "pq9ish.h"

extern struct watchdog hwdg;
extern struct pq9ish hpq9ish;
extern osTimerId telemetry_timerHandle;

int
fsm_task()
{
	uint8_t wdgid = 0;
	fsm_state_t last_state = FSM_TC_MODE;
	osTimerStart(telemetry_timerHandle, PQ9ISH_TELEMETRY_INTERVAL);
	watchdog_register(&hwdg, &wdgid, "fsm");
	osDelay(4);
	/* Infinite loop */
	for (;;) {
		watchdog_reset_subsystem(&hwdg, wdgid);
		switch (hpq9ish.fsm_state) {
			case FSM_TC_MODE:
				if (hpq9ish.status.power_save) {
					hpq9ish.fsm_state = FSM_LOW_POWER_MODE;
					break;
				}
				if (last_state != FSM_TC_MODE) {
					osTimerStart(telemetry_timerHandle, PQ9ISH_TELEMETRY_INTERVAL);
					last_state = FSM_TC_MODE;
				}
				break;
			case FSM_LOW_POWER_MODE:
				if (!hpq9ish.status.power_save) {
					hpq9ish.fsm_state = FSM_TC_MODE;
					break;
				}
				if (last_state != FSM_LOW_POWER_MODE) {
					osTimerStart(telemetry_timerHandle, PQ9ISH_TELEMETRY_INTERVAL_LP);
					last_state = FSM_LOW_POWER_MODE;
				}
				break;
			default:
				break;
		}
		osDelay(100);
	}
}
