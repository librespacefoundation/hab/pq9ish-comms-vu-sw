/*
 *  A PQ9 based Weather Station
 *
 *  Copyright (C) 2017 Libre Space Foundation
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "pq9ish.h"
#include "weather.h"
#include "utils.h"
#include "gps.h"
#include <string.h>
#include "error.h"
#include "balloon.h"
extern uint32_t TxMailbox;
extern CAN_HandleTypeDef hcan1;
extern uint8_t TxData[8];
extern balloon_system_conf_t system_conf;
extern GPS_Data gps_data;

int
weather_telem_reset(weather_data_t *w)
{
	if (!w) {
		return -INVAL_PARAM;
	}
	w->atmo_internal_temp = 0;
	w->atmo_system_time = 0;
	w->batt_milivolt = 0;
	w->external_temp1 = 0;
	w->external_temp2 = 0;
	w->flightPhase = 0;
	w->gas_co = 0;
	w->humidity = 0;
	w->muon_count = 0;
	w->muon_level = 0;
	w->pressure = 0;
	w->pressure_altitude = 0;
	w->system_state = 0;
	w->temperature = 0;
	w->verticalVelocity = +0;
	return NO_ERROR;
}

int
weather_update_atmo(weather_data_t *w, pq_atmo_atmospheric_data_t *data)
{
	if (!w) {
		return -INVAL_PARAM;
	}

	w->pressure = data->pressure;
	w->temperature = data->temperature;
	w->humidity = data->humidity;
	w->pressure_altitude = data->pressure_altitude;

	return NO_ERROR;
}

int
weather_update_muon(weather_data_t *w, pq_atmo_sensor_t *data)
{
	if (!w) {
		return -INVAL_PARAM;
	}

	//w->muon_level = data->muon_level;
	//w->muon_count = data->muon_count;
	//w->gas_co = data->gas_CO;
	//w->atmo_internal_temp = data->mcu_temp;

	return NO_ERROR;
}

int
weather_update_system_state(weather_data_t *w, state_data_t *data)
{
	if (!w) {
		return -INVAL_PARAM;
	}

	w->verticalVelocity = data->verticalVelocity;
	w->system_state = (data->system_state & 0x0F) | (w->system_state & 0xF0);
	w->flightPhase = data->flightPhase;

	return NO_ERROR;
}

int
atmo_request_handler()
{
	//TODO merge system_conf and init_data
	initialize_data_t init_data;
	pq_comms_message_t
	data; //other end expects uint16(pyro), uint16(highest), uint8(missPhase)

	init_data.pyro_alt = system_conf.pyro_alt;
	init_data.highestLandPoint = system_conf.highest_land_point;
	//TODO set mission_phase to latest record, or 0
	init_data.mission_phase = 0;
	data.value1 = init_data.pyro_alt;
	data.value2 = init_data.highestLandPoint;
	data.value3 = init_data.mission_phase;
	data.value4 = system_conf.QNH;
	data.message_type = PQ_ATMO_INIT_DATA;
	memcpy(&TxData, &data, 8);

	CAN_TxHeaderTypeDef TxHeader;
	TxHeader.StdId = (PQ_COMMBC_NODE_ID << 6) | data.message_type;
	TxHeader.ExtId = 0;
	TxHeader.RTR = CAN_RTR_DATA;
	TxHeader.IDE = CAN_ID_STD;
	TxHeader.DLC = sizeof(data) - 2;
	TxHeader.TransmitGlobalTime = DISABLE;

	while (HAL_CAN_IsTxMessagePending(&hcan1, TxMailbox)) {}
	if (HAL_CAN_AddTxMessage(&hcan1, &TxHeader, TxData, &TxMailbox)
	    != HAL_OK) {
//                              //TODO: Handle error of single device on bus
		Error_Handler();
	}
}

int
send_gps_altitude()
{
	pq_comms_message_t data;
	data.value1 = gps_data.alt;
	data.message_type = PQ_GPS_DATA;
	memcpy(&TxData, &data, 2);

	CAN_TxHeaderTypeDef TxHeader;
	TxHeader.StdId = PQ_COMMBC_NODE_ID | data.message_type;
	TxHeader.ExtId = 0;
	TxHeader.RTR = CAN_RTR_DATA;
	TxHeader.IDE = CAN_ID_STD;
	TxHeader.DLC = sizeof(data) - 2;
	TxHeader.TransmitGlobalTime = DISABLE;

	while (HAL_CAN_IsTxMessagePending(&hcan1, TxMailbox)) {}
	if (HAL_CAN_AddTxMessage(&hcan1, &TxHeader, TxData, &TxMailbox)
	    != HAL_OK) {
		//TODO: Handle error of single device on bus
		Error_Handler();
	}
}

int
atmo_update_sensor2(weather_data_t *w, pq_atmo_sensor2_t *data)
{
	w->batt_milivolt = data->batt;
	return NO_ERROR;

}
