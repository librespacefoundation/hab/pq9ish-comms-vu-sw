/*
 *  A PQ9 based Weather Station
 *
 *  Copyright (C) 2017,2018 Libre Space Foundation
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "balloon.h"
#include "ff.h"
#include "fatfs.h"
#include "weather.h"
#include "gps.h"

extern RTC_DateTypeDef sDate;
extern RTC_TimeTypeDef sTime;
extern RTC_HandleTypeDef hrtc;
extern GPS_Data gps_data;

uint8_t
logSystemStatus(pq9ish_system_status_t data)
{
	int res;
	FIL FLog;

	res = f_open(&FLog, "SYS/SYSTEM.LOG", FA_OPEN_APPEND | FA_WRITE);
	if (res != FR_OK)
		return res;

	if (res != FR_OK)
		return res;

	HAL_RTC_GetTime(&hrtc, &sTime, RTC_FORMAT_BIN);
	HAL_RTC_GetDate(&hrtc, &sDate, RTC_FORMAT_BIN);

	res = f_printf(&FLog,
	               "%u-%02u-%02uT%02u:%02u:%02u: Battery: %f V, Solar: %f V\n",
	               2000 + sDate.Year, sDate.Month, sDate.Date, sTime.Hours,
	               sTime.Minutes, sTime.Seconds, data.sysTick, data.battVolt,
	               data.inputVolt);
	if (res == -1)
		return res;

	f_close(&FLog);
	return FR_OK;
}

uint8_t
logSensorData(weather_data_t data)
{
	int res;
	FIL FLog;
	FILINFO finfo;

	res = f_stat("DATA/SENSOR.csv", &finfo);

	if (res != FR_OK) {
		res = f_open(&FLog, "DATA/SENSOR.csv", FA_CREATE_NEW | FA_WRITE);
		res = f_printf(&FLog,
		               "Timestamp,Lat,Lon,Alt,T,Humidity,Pressure,GAS CO,Muon Level,Muon count\n");
	} else {
		res = f_open(&FLog, "DATA/SENSOR.csv", FA_OPEN_APPEND | FA_WRITE);
		if (res != FR_OK)
			return res;
	}

	HAL_RTC_GetTime(&hrtc, &sTime, RTC_FORMAT_BIN);
	HAL_RTC_GetDate(&hrtc, &sDate, RTC_FORMAT_BIN);
	res =
	        f_printf(
	                &FLog,
	                "%u-%02u-%02uT%02u:%02u:%02u,%s,%s,%s,%d,%d,%d,%d,%d,%d\n",
	                2000 + sDate.Year, sDate.Month, sDate.Date, sTime.Hours,
	                sTime.Minutes, sTime.Seconds, gps_data.lat, gps_data.lon, gps_data.alt,
	                data.temperature, data.humidity,
	                data.pressure, data.gas_co, data.muon_level, data.muon_count);

	f_close(&FLog);
	if (res == -1)
		return res;
	return FR_OK;
}
