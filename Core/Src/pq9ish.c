/*
 *  PQ9ISH COMMS: comms module, part of an open source pocketqube stack
 *
 *  Copyright (C) 2020, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "pq9ish.h"
#include "error.h"
#include "conf.h"
#include "telemetry.h"
#include "cmsis_os.h"
#include <string.h>

extern uint8_t reset_flag;
typedef enum {
	RESET_CAUSE_UNKNOWN = 0,
	RESET_CAUSE_LOW_POWER_RESET,
	RESET_CAUSE_WINDOW_WATCHDOG_RESET,
	RESET_CAUSE_INDEPENDENT_WATCHDOG_RESET,
	RESET_CAUSE_SOFTWARE_RESET,
	RESET_CAUSE_POWER_ON_POWER_DOWN_RESET,
	RESET_CAUSE_EXTERNAL_RESET_PIN_RESET,
	RESET_CAUSE_BROWNOUT_RESET,
} reset_cause_t;



void
pq9ish_update_ror_counters(struct pq9ish *q);

static void
default_settings(struct pq9ish_settings *s)
{
	s->init = PQ9ISH_FLASH_MAGIC_VAL;
	s->sat_id = PQ9ISH_SAT_ID;
	s->rx_freq = PQ9ISH_RX_FREQ_HZ;
	s->tx_freq = PQ9ISH_TX_FREQ_HZ;

	s->tm_resp_tx_power = PQ9ISH_DEFAULT_TX_POWER;
	s->tm_resp_enc = RADIO_ENC_RS;
	s->tm_resp_mod = RADIO_MOD_FSK;
	s->tm_resp_baud = RADIO_BAUD_9600;
	s->tm_resp_pa_en = 1;
	s->reset_counters.brownout_counter = 0;
	s->reset_counters.independent_watchdog_counter = 0;
	s->reset_counters.low_power_counter = 0;
	s->reset_counters.software_counter = 0;
	s->mute_flag = 0;
#if PQ9ISH_SN==0
	uint8_t sha[24] = {0x11, 0x18, 0x12, 0xff, 0x8a, 0xc7, 0xea, 0xb7, 0x3c, 0x21,
	                   0xcc, 0x12, 0x65, 0xdf, 0x4a, 0x97, 0xef, 0xfb, 0x7b, 0x3f, 0x78, 0x6e, 0x55, 0x56
	                  };
//	uint8_t sha[24] = {0xf4, 0x34, 0x2e, 0x68, 0x74, 0xfd, 0xee, 0xe0, 0xe4, 0x54,
//	                   0x52, 0x5f, 0x91, 0x11, 0x73, 0x59, 0xb3, 0x3f, 0xc5, 0xa3, 0x78, 0x65, 0xf9, 0x56
//	                  };
	memcpy(s->sha256, sha, 24 * sizeof(uint8_t));
#endif
#if PQ9ISH_SN==1
	uint8_t sha[24] = {0x11, 0x18, 0x12, 0xff, 0x8a, 0xc7, 0xea, 0xb7, 0x3c, 0x21,
	                   0xcc, 0x12, 0x65, 0xdf, 0x4a, 0x97, 0xef, 0xfb, 0x7b, 0x3f, 0x78, 0x6e, 0x55, 0x56
	                  };
	memcpy(s->sha256, sha, 24 * sizeof(uint8_t));
#endif
}

int
pq9ish_init(struct pq9ish *q)
{
	if (!q) {
		return -INVAL_PARAM;
	}
	memset(q, 0, sizeof(struct pq9ish));

	/* Wait power to stabilize before touching the flash */
	HAL_Delay(1000);

	/* Initialize the fault tolerant storage engine */
	int ret = ft_storage_init(&q->ft_persist_mem,
	                          sizeof(struct pq9ish_settings),
	                          PQ9ISH_STORAGE_FLASH_ADDR);
	if (ret) {
		return ret;
	}
	ret = pq9ish_read_settings(q);
	if (ret) {
		return ret;
	}

	// Update ROR counter fields
	q->reason_of_reset = reset_flag;
	pq9ish_update_ror_counters(q);
	// Write back to storage
	pq9ish_write_settings(q);

	/* Initialize radio related structures */
	ret = radio_init(&q->hradio);
	if (ret) {
		return ret;
	}


	return NO_ERROR;
}

int
pq9ish_read_settings(struct pq9ish *q)
{
	if (!q) {
		return -INVAL_PARAM;
	}
	int ret = ft_storage_read(&q->ft_persist_mem, &q->settings);
	/* In case the read failed, restore the defaults and write back the memory*/
	if (ret) {
		default_settings(&q->settings);
		return pq9ish_write_settings(q);
	}
	/* Maybe the flash was not initialized */
	if (q->settings.init != PQ9ISH_FLASH_MAGIC_VAL) {
		default_settings(&q->settings);
		return pq9ish_write_settings(q);
	}
	return NO_ERROR;
}

int
pq9ish_write_settings(struct pq9ish *q)
{
	if (!q) {
		return -INVAL_PARAM;
	}
	return ft_storage_write(&q->ft_persist_mem, &q->settings);
}

void
pq9ish_update_ror_counters(struct pq9ish *q)
{
	if (q->reason_of_reset & (1UL << (RESET_CAUSE_LOW_POWER_RESET))) {
		q->settings.reset_counters.low_power_counter++;
	}
	if (q->reason_of_reset & (1UL << (RESET_CAUSE_INDEPENDENT_WATCHDOG_RESET))) {
		q->settings.reset_counters.independent_watchdog_counter++;
	}
	if (q->reason_of_reset & (1UL << (RESET_CAUSE_SOFTWARE_RESET))) {
		q->settings.reset_counters.software_counter++;
	}
	if (q->reason_of_reset & (1UL << (RESET_CAUSE_BROWNOUT_RESET))) {
		q->settings.reset_counters.brownout_counter++;
	}
}
